# Сервис рассылок уведомлений

## Определение задания

API реализует функционал рассылки сообщений исходя из данных клиентов.

API сервис реализуется на базе Django Rest Framework.

## Системные требования

- Python 3.6+
- Windows/MacOS/Linux

## Использованные технологии

- Python 3.9
- Django 4.0.8
- Django Rest Framework
- PostreSQL
- gunicorn

## Установка проекта


Клонировать репозиторий:

```bash
git clone https://gitlab.com/rakhat.erezhepov02/fr_test.git

cd FR_test_task
```

Установить требования
```bash 
cd test_work
pip install -r requirements.txt
```

Cоздать и открыть файл `.env` с переменными окружения:

```bash
cd ..
cd infra

touch .env
```

Включите `.env` файл переменные окружения по этому списку следуя примеру:

```bash
BROKER=redis://redis
BROKER_URL=redis://redis:6379/0
DB_ENGINE=django.db.backends.postgresql
DB_NAME=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_USER=postgres
DB_HOST=db
DB_PORT=5432
SENDING_API_TOKEN=<токен для задания>

```


## Documentation

Перейти по ссылке для обзора документации:

`http://127.0.0.1/swagger/`

`http://127.0.0.1/redoc/`
